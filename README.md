# Ronils

![Logo](logo.png)

Ronils is a tool to manage meetings such as parliaments and committees written in python.
Features of Ronils are

* display of attendant persons
* protocoling of events
* list of speakers
* ballot managing

and many more.

The tool is written to be useful in the [Student Body Parliament (StuPa)](https://stupa.uni-goettingen.de/) of the [University of Göttingen](https://uni-goettingen.de/).

## Table of Content

0. [Table of Content](#table-of-content)
1. [Installatin](#installation)
1. [Usage](#Usage)
1. [Configuration](#configuration)
1. [Example Usecase](#example-usecase)
1. [Customisation](#customisation)
1. [Documentation](#documentation)
1. [Future Versions](#future-versions)
1. [Licencse](#licencse)

## Intallation

To use this software you can simply download this repository and start main.py if the following raquirements are satisfied.

```
git clone git@gitlab.gwdg.de:felix.schelle1/ronils.git
```

Future versions will be available as a .tar archive.

### Requirements

* python 3.6+
* tk

To check if the requirements are installed correctly you can check if you can start the tool using the exmaple data.

```
main.py data/data_example.json
```

## Usage

To see help, run:

```
main.py -h
```

## Configuration

You probably want to do some configurations before you use this tool.
All configurations are done in json.

*todo explain configurations*

## Example Usecase

The setup that is used in the StuPa is the following:

* Ronils runs on a Raspberry Pi 400 Kit with Linux
* 2 screens connected to the Rasberry Pi
    1. Screen with control window open (Monitor)
    1. Screen with view Window in fullscreen (Beamer)
* Setup is completely offline

*todo photo of setup in use*

## Customisation

Ronils is written very modular.
You can build your own parliament logic or even write your own GUI.
In future versions the customisation will be made simpler.

More about customisation can be read in the documentation.

## Documentation

The technical documentation of this software will be available online soon.
*todo write documention*

## hafee support

Ronils comes with ready hafee support.
*todo write usage of hafee*

## Future Versions

Planned features that may exist in future versions.

* Importing and searching in legal texts
* Full automated generation of protocols
* Full automated generation of resolutions
* Managing the TO
* Check-in of guests

### GUI

* hafee control
* Multilanguage support
* list of speakers
* Visualisierung ob StuPa stimmberechtigt und ob 2/3-Mehrheit möglich
* automatische Berechnung welche Fraktion in welcher Spalte steht

## Licencse

**AGPL-3.0-or-later**  
![AGPLv3](agplv3.png)  
see [License](LICENSE) for more information.  

Copyright (c) 2019-2022, Felix Schelle.
