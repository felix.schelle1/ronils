#!/usr/bin/env python3

import math, random
from time import strftime

import fileio

class Stupa:
    def __init__(self, session_name, parliament_data, config):
        """
        Initialize new session with given parameters.
        :param str session_name: Name of the session
        :param parliament_data: dict containing parliament data 
        :param config: dict containing configuration
        """
        self.session_name = fileio.sanatise_filename(session_name)

        self.parliament_data = parliament_data
        self.factions = parliament_data['factions']
        self.pseudonyms = parliament_data['pseudonyms']

        self.config = config
        cf = self.config['files']
        for key in cf.keys():
            cf[key] = cf[key].replace('$s', self.session_name)
            # more placeholders in future versions

        if cf['base_path'][-1] != '/':
            cf['base_path'] += '/'
        bp = cf['base_path']
        self.paths = {
                "base":         bp,
                "votes":        bp + self.config['files']['votes'],
                "protocol":     bp + self.config['files']['protocol'],
                "attendance":   bp + self.config['files']['attendance'],
            }
        fileio.invoke_session(self.paths)

        self.attendant = {}
        for f_name in self.factions.keys():
            self.attendant[f_name] = []

        self.speakers_list = []

        self.time_pattern = self.config['time_pattern'] # default = '%Y-%m-%d %H:%M %z'
        self.now = ""
        self.update_time()

    def speaker_pop(self, position=0, name=''):
        if name:
            self.speakers_list.remove(name)
        else:
            if position < len(self.speakers_list):
                self.speakers_list.pop(position)
        return

    def speaker_push(self, name):
        self.speakers_list.append(name)
        return

    def update_time(self, pattern=''):
        if not pattern:
            pattern = self.time_pattern
        self.now = strftime(pattern)

    def arrive(self, person, write_to_file=True):
        """
        Sets the state of a person to attendant.
        :param person: 3-tuple identifying a person by faction, name, rank
        :param write_to_file: If set to True change is written to file
        """
        # todo support multiple arrivels at one time
        # todo handle guest faction
        f_name = person[0]
        member = person[1]
        rank   = person[2]

        if not member in self.factions[f_name]['members']:
            print(f"Warning: {member} not in faction {faction_name}.")
            return
        if (member, rank) in self.attendant[f_name]:
            # print(f"Warning: {member} already attendant.")
            return
        self.attendant[f_name].append((member, rank))
        # keep attendent persons sorted by rank
        self.attendant[f_name].sort(key = lambda m: m[1])
        if write_to_file:
            self.update_time()
            fileio.record_attendance( timestamp = self.now,
                                      person = person,
                                      filepath = self.paths['attendance'],
                                      arriving=True)

    def leave(self, person, write_to_file=True):
        """
        Sets the state of a person to not attendant.
        :param person: 3-tuple identifying a person by faction, name, rank
        :param write_to_file: If set to True change is written to file
        """
        f_name = person[0]
        member = person[1]
        rank   = person[2]

        if not (member, rank) in self.attendant[f_name]:
            # print(f"Warning: {member} was not attendant.")
            return
        self.attendant[f_name].remove((member, rank))
        if write_to_file:
            self.update_time()
            fileio.record_attendance( timestamp = self.now,
                                      person = person,
                                      filepath = self.paths['attendance'],
                                      arriving=False)

    def attendance_change(self, person, write_to_file=True):
        """
        Changes the attendance state of a person.
        :param person: 3-tuple identifying a person by faction, name, rank 
        :param write_to_file: If set to True change is written to file
        """
        f_name = person[0]
        member = person[1]
        rank = rank[2]

        if (member, rank) in self.attendant[f_name]:
            self.leave( person=person, write_to_file=write_to_file)
        else:
            self.arrive(person=person, write_to_file=write_to_file)

    def arrive_from_backup(self, filepath=''):
        """
        Sets states of persons according to given file.
        :param filepath: path to csv file containing attendance data (faction;name;rank;state)
        """
        if not filepath:
            filepath = self.paths['attendance']
        backup_list = fileio.arrive_from_backup(filepath=filepath)

        for change in backup_list:
            if change[3] == 'True': # False is 'False' and therefore gives True
                                    # in future more values may be accepted as True
                self.arrive((change[0], change[1], int(change[2])), write_to_file=False)
            else:
                self.leave( (change[0], change[1], int(change[2])), write_to_file=False)

    def export_attendance(self):
        fileio.export_attendance(filepath=self.paths['attendance'], end="Ende")

    def sized_factions(self, offset=0):
        # offset < 0 allowed. minimal used size of a faction is 0.
        factions_sized = {}
        for f_name, f_attendant in self.attendant.items():
            f_size = min(max(0, self.factions[f_name]['size'] + offset), len(f_attendant))
            factions_sized[f_name] = f_attendant[:f_size]
        return factions_sized

    def quorum_satisfied(self, threshold=.5):
        # todo: bug may exist. ceiling/flooring may be wrong.
        # quorum_satisfactoion = attendant / seats >= threshold
        f_trim = self.sized_factions()
        voting_power = 0
        for members in f_trim.values():
            voting_power += len(members)
        satisfied = voting_power >= math.ceil(self.parliament_data['size'] * threshold)
        return satisfied

    def new_ballot(self, ballot_id='', vote_type='by_faction', vote_options=['ja', 'nein', 'Enthaltung', 'ungültig'], votes_per_person=1, print_ballot=False):
        """
        Generate an iterateble of voters and a list of vote options and returns them.
        To record the vote result use record_ballot_result().
        Attention: This function can only used to generates ballots. The process of voting itself has to be made without computers! It is impossible to develop a voting machine that fullifies democratic principles!!
        """
        valid_vote_types = ['by_faction', 'by_name', 'secret']
        
        if not ballot_id:
            self.update_time()
            ballot_id = self.now

        if vote_type not in valid_vote_types:
            print(f"Warning: Invalid vote type {vote_type}.\n Falling back to default 'by_faction'.")
            vote_type = 'by_faction'

        """
        None is implicit by missing vote
        if None not in vote_options:
            vote_options.append(None)
            # None is the value if someone does not vote who would have to
            # for example if the person accidently leaves the room to get covid vaccine
            # or if no member of a faction is attendant
        """

        if votes_per_person < 1:
            print(f"Warning: votes_per_person must be > 0, is {votes_per_person}\nfalling back to default 1.")
            votes_per_person = 1

        if vote_type == 'by_faction':
            voters = list(self.factions.keys())
        elif vote_type == 'by_name':
            f_trim = self.sized_factions()
            voters = []
            for f_name, members in f_trim.items():
                voters.extend([(f_name, m) for m in members])
        elif vote_type == 'secret':
            voters = ['Parliament']
        else:
            print(f"Something went wrong. Invalid vote type {vote_type}.\nThis should not be possible.\nPlease report a bug describing what you have done to end up here.")
            # should I throw exception here? I guess that makes sense. Will do in future.
            return False

        voters.sort()
        # shuffled voters print because some people wanted this:
        if vote_type == 'by_faction':
            voters_shuffled = voters.copy()
            random.shuffle(voters_shuffled)
            print("shuffled voters list:", voters_shuffled)

        return voters, vote_options

    def record_vote_result(self, ballot_id, result, filepath='', threshold=0):
        if not filepath:
            filepath = self.paths['votes']
        fileio.record_vote( ballot_id = ballot_id,
                            result = result,
                            filepath = filepath)
        if threshold <= 1:
            # was relative vote. calc absolute threshold value from number of attendent persons
            pass
        # todo return vote result calculated from threshold
        return threshold

if __name__ == "__main__":
    print("not runnable. import this file")
