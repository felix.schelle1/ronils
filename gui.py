#!/usr/bin/env python3

import tkinter as tk
import tkinter.ttk as ttk
# import tkinter.font as tkf
from time import strftime
# from math import floor

from queue import Empty as QEmpty


class Gui:

    def __init__(self, meeting, server=None):

        self.meeting = meeting
        self.server = server
        # if server: server starts at end of __init__

        def set_styles():
            self.root.option_add('*Font', 'bold 20')
            style = ttk.Style(self.root)
            style.theme_use('alt')
            # self.view.attributes("-fullscreen", 1)

            # styles todo
            # s = ttk.Style()
            # ttk.Frame(ctl_mainframe, width=200, height=200, style='Danger.TFrame').grid()

        def load_icons():
            self.icons = {
                    'event':    tk.PhotoImage(file='icons/event_64.png'),
                    'logo':     tk.PhotoImage(file='logo.png'),
                    'people_1': tk.PhotoImage(file='icons/people_1_64.png'),
                    'people_2': tk.PhotoImage(file='icons/people_2_64.png'),
                    'people_3': tk.PhotoImage(file='icons/people_3_64.png'),
                    'sergio':   tk.PhotoImage(file='icons/sergio_64.png'),
                    'speak':    tk.PhotoImage(file='icons/speak_64.png'),
                    'undo':     tk.PhotoImage(file='icons/undo_64.png'),
                    'vote':     tk.PhotoImage(file='icons/vote_64.png')
            }

        def build_view_window():
            view_mainframe = ttk.Frame(self.view, padding="20 20 20 20")
            view_mainframe.grid(column=0, row=0, sticky=(tk.N, tk.W, tk.E, tk.S))
            view_mainframe.columnconfigure(0, weight=1)
            view_mainframe.rowconfigure(2, weight=1)

            def build_view_top():
                view_top = ttk.Frame(view_mainframe)
                view_top.grid(column=0, row=0, columnspan=3, rowspan=1, sticky=(tk.N, tk.W, tk.E))
                view_top.columnconfigure(0, weight=0)
                view_top.columnconfigure(1, weight=1)
                view_top.columnconfigure(2, weight=0)
                view_top.rowconfigure(0, weight=0)

                ttk.Label(view_top, text=self.meeting.parliament_data['name']).grid(column=0, row=0, sticky=(tk.N, tk.W))
                ttk.Label(view_top, text=self.meeting.session_name).grid(column=1, row=0, sticky=tk.NW)

                self.time = tk.StringVar()
                self.view_time = ttk.Label(view_top, textvariable=self.time)
                self.view_time.grid(column=2, row=0, sticky=(tk.N, tk.E))

                ttk.Separator(view_mainframe, orient=tk.HORIZONTAL).grid(column=0, row=1, columnspan=3, sticky=(tk.W, tk.E))

            def build_view_bottom():
                # view_bottom = ttk.Panedwindow(view_mainframe, orient=tk.HORIZONTAL)
                view_bottom = ttk.Frame(view_mainframe)
                view_bottom.grid(column=0, row=2, sticky=(tk.N, tk.S, tk.W, tk.E))
                view_bottom.columnconfigure(0, weight=1)
                view_bottom.columnconfigure(1, weight=1)
                view_bottom.columnconfigure(2, weight=1)
                view_bottom.rowconfigure(0, weight=1)

                view_bottom_left = ttk.Frame(view_bottom)
                view_bottom_left.grid(column=0, row=0, sticky=(tk.N, tk.W, tk.E))
                view_bottom_left.columnconfigure(0, weight=1)
                view_bottom_left.rowconfigure(0, weight=1)
                view_bottom_left.rowconfigure(1, weight=1)
                view_bottom_left.rowconfigure(2, weight=1)
                view_bottom_left.rowconfigure(3, weight=1)

                view_bottom_middle = ttk.Frame(view_bottom)
                view_bottom_middle.grid(column=1, row=0, sticky=(tk.N, tk.W, tk.E))
                view_bottom_middle.columnconfigure(0, weight=1)
                view_bottom_middle.rowconfigure(0, weight=1)
                view_bottom_middle.rowconfigure(1, weight=1)
                view_bottom_middle.rowconfigure(2, weight=1)
                view_bottom_middle.rowconfigure(3, weight=1)

                view_bottom_right = ttk.Frame(view_bottom)
                view_bottom_right.grid(column=2, row=0, sticky=(tk.N, tk.W, tk.E))
                view_bottom_right.columnconfigure(0, weight=1)
                view_bottom_right.rowconfigure(0, weight=1)

                #view_bottom.add(view_bottom_left)
                #view_bottom.add(view_bottom_middle)
                #view_bottom.add(view_bottom_right)

                #ttk.Label(view_bottom, text="TEST:").grid(column=0, row=0, sticky=(tk.N, tk.W))

                self.attendant_members_strings = {}
                view_bottom_positions = [view_bottom_left, view_bottom_middle, view_bottom_right]
                for f in self.meeting.factions:
                    self.attendant_members_strings[f] = tk.StringVar()
                    f_vbp = view_bottom_positions[self.meeting.factions[f]['column']]
                    lf = ttk.Labelframe(f_vbp, text=f"{f} ({self.meeting.factions[f]['size']})")
                    lf.columnconfigure(0, weight=1)
                    lf.rowconfigure(0, weight=1)
                    lf.grid(sticky=(tk.N, tk.W, tk.E))
                    
                    label = ttk.Label(lf, textvariable=self.attendant_members_strings[f])
                    label.columnconfigure(0, weight=1)
                    label.rowconfigure(0, weight=1)
                    label.grid(sticky=tk.NW)

            build_view_top()
            build_view_bottom()

        def build_ctl():
            ctl_mainframe = ttk.Frame(self.ctl, padding="10 10 10 10")
            ctl_mainframe.grid(column=0, row=0, sticky=(tk.N, tk.W, tk.E, tk.S))
            ctl_mainframe.columnconfigure(0, weight=1)
            ctl_mainframe.rowconfigure(0, weight=1)

            """
            tmp_s = ttk.Scrollbar(ctl_mainframe, orient=tk.VERTICAL, command=ctl_mainframe.yview)
            #   s = ttk.Scrollbar(fframe, orient=tk.VERTICAL, command=self.ctl_members_guilists[f].yview)
            tmp_s.grid(column=1, row=0, sticky=tk.NS, pady=5)
            #   s.grid(column=1, row=0, sticky=tk.NS, pady=5)
            ctl_mainframe['yscrollcommand'] = tmp_s.set
            #self.ctl_members_guilists[f]['yscrollcommand'] = s.set
            """

            ttk.Label(ctl_mainframe, text="{logo} Ronils meeting controller", image=self.icons['logo'], compound='left').grid(column=0, row=0, sticky=tk.NW)
            # todo add frame around edit buttons
            # ttk.Button(ctl_mainframe, text="Undo", image=self.icons['undo'], compound='left', command=self.tmpnop).grid(column=0, row=1, sticky=tk.W)

            # === ctl tabs creation ===
            ctl_tabs = ttk.Notebook(ctl_mainframe, padding="5 5 10 10")
            #ctl_tabs.columnconfigure(0, weight=1)
            #ctl_tabs.rowconfigure(0, weight=1)
            ctl_members     = ttk.Frame(ctl_tabs, padding="5 5 10 10")
            ctl_speakers    = ttk.Frame(ctl_tabs, padding="5 5 10 10")
            ctl_votes       = ttk.Frame(ctl_tabs, padding="5 5 10 10")
            ctl_events      = ttk.Frame(ctl_tabs, padding="5 5 10 10")
            ctl_preferences = ttk.Frame(ctl_tabs, padding="5 5 10 10")
            ctl_sergio      = ttk.Frame(ctl_tabs, padding="5 5 10 10")

            ctl_tabs.add( ctl_members,      text="Members",     image=self.icons['people_3'],   compound='left')
            ctl_tabs.add( ctl_speakers,     text="Speakers",    image=self.icons['speak'],      compound='left')
            ctl_tabs.add( ctl_votes,        text="Votes",       image=self.icons['vote'],       compound='left')
            ctl_tabs.add( ctl_events,       text="Events",      image=self.icons['event'],      compound='left', state="disabled")
            ctl_tabs.add( ctl_sergio,       text="Sergio",      image=self.icons['sergio'],     compound='left', state="disabled")
            ctl_tabs.add( ctl_preferences,  text="Preferences", image=self.icons['undo'],       compound='left')
            ctl_tabs.grid(column=0, row=2, sticky=(tk.N, tk.W, tk.E, tk.S))

            # ctl_output
            self.ctl_output_string = tk.StringVar()
            self.ctl_output = ttk.Label(ctl_mainframe, textvariable=self.ctl_output_string)
            self.ctl_output.grid(sticky=(tk.S, tk.W, tk.E))

            def build_tab_members():
                self.member_lut = {}
                # member lookup-table to enable alphabetic sorting in gui
                for f_name in self.meeting.factions.keys():
                    f_members = self.meeting.factions[f_name]['members']
                    self.member_lut[f_name] = [m[0] for m in sorted(enumerate(f_members), key=lambda m: m[1])]
                    # list with ranks of faction members after sorting their names alphabetic 

                self.ctl_members_factions = ttk.Notebook(ctl_members, padding="5 5 5 5")
                self.ctl_members_guilists = {}
                for f in sorted(self.meeting.factions.keys()):
                    fframe = ttk.Frame(self.ctl_members_factions)
                    fframe.columnconfigure(0, weight=1)
                    fframe.rowconfigure(0, weight=1)
                    # self.ctl_members_factions.add(fframe, text=f, image=self.icons['people_3'], compound='left')
                    self.ctl_members_factions.add(fframe, text=f)
                    # f_members = tk.StringVar(value = sorted([f"{_[0]} {_[1]}" for _ in self.meeting.factions[f]['members']]))
                    f_members = tk.StringVar(value = sorted(self.meeting.factions[f]['members']))
                    self.ctl_members_guilists[f] = tk.Listbox(fframe, height=15, listvariable=f_members, selectmode='extended')
                    self.ctl_members_guilists[f].grid(column=0, row=0, sticky=(tk.N, tk.S, tk.W, tk.E), pady=5)
                    s = ttk.Scrollbar(fframe, orient=tk.VERTICAL, command=self.ctl_members_guilists[f].yview)
                    s.grid(column=1, row=0, sticky=tk.NS, pady=5)
                    self.ctl_members_guilists[f]['yscrollcommand'] = s.set
                self.ctl_members_factions.grid(column=0, row=0, sticky=(tk.N, tk.W, tk.E, tk.S))

                # buttons for arrival
                buttonframe = ttk.Frame(ctl_members)
                buttonframe.grid(column=1, row=0, sticky=tk.NE)

                ttk.Button(buttonframe, text='Eintragen', command=self.members_arrive).grid()
                ttk.Button(buttonframe, text='Austragen', command=self.members_leave ).grid()

                ttk.Separator(buttonframe, orient=tk.HORIZONTAL).grid(columnspan=1, sticky=(tk.W, tk.E), pady=5)
                self.register_queue_count = tk.StringVar()
                rqc_label = ttk.Label(buttonframe, textvariable=self.register_queue_count).grid()
                ttk.Button(buttonframe, text='Registrieren',    command=self.register_queue_handle).grid()
                ttk.Button(buttonframe, text='Verwerfen',       command=self.register_queue_delete).grid()

                ttk.Separator(buttonframe, orient=tk.HORIZONTAL).grid(columnspan=1, sticky=(tk.W, tk.E), pady=5)
                ttk.Button(buttonframe, text='Backup laden',    command=self.members_from_backup).grid()
                ttk.Button(buttonframe, text='Exportieren',     command=self.export_attendance  ).grid()

                ttk.Separator(buttonframe, orient=tk.HORIZONTAL).grid(columnspan=1, sticky=(tk.W, tk.E), pady=5)
                ttk.Button(buttonframe, text='Quorum 1/2?', command=self.quorum_satisfied   ).grid()
                ttk.Button(buttonframe, text='Quorum 2/3?', command=self.quorum_satisfied_23).grid()
                
                #ttk.Label(ctl_members, text="Members Search").grid(column=0, row=0, sticky=tk.W)
                #ttk.Entry(ctl_members, width=20).grid(column=1, row=0, sticky=tk.W)

            def build_tab_speakers():
                sframe_top = ttk.Frame(ctl_speakers)
                sframe_top.columnconfigure(0, weight=1)
                sframe_top.rowconfigure(0, weight=1)
                sframe_top.grid(column=0, row=0, sticky=(tk.N, tk.W, tk.E))

                self.speaker_entry = ttk.Entry(sframe_top, width=20)
                self.speaker_entry.grid(column=0, row=0, sticky=tk.NW)
                ttk.Button(sframe_top, text='Eintragen', command=self.speakers_push).grid(column=1, row=0, sticky=tk.N)
                ttk.Button(sframe_top, text='Austragen', command=self.speakers_pop).grid(column=2, row=0, sticky=tk.N)

            def build_tab_votes():
                vframe_0 = ttk.Labelframe(ctl_votes, text="New ballot", padding="5 5 10 10")
                vframe_0.columnconfigure(0, weight=1)
                vframe_0.rowconfigure(0, weight=1)
                vframe_0.rowconfigure(1, weight=0)
                vframe_0.rowconfigure(2, weight=0)
                vframe_0.rowconfigure(3, weight=1)
                # todo move these rowconfigures to parent thing
                vframe_0.grid(column=0, row=0, sticky=(tk.N, tk.W, tk.E))

                self.ballot_id_entry_text = tk.StringVar()
                ballot_id_entry = ttk.Entry(vframe_0, width=20, textvariable=self.ballot_id_entry_text).grid(column=0, row=0, sticky=tk.NW)

                # num of opts/num of smth
                self.vote_spinbox_value = tk.StringVar()
                ttk.Spinbox(vframe_0, from_=1, to=100, textvariable=self.vote_spinbox_value, width=3).grid(column=1, row=0, sticky=tk.NW)

                # check https://tkdocs.com/tutorial/widgets.html for select usage
                self.ballot_type_select_value = tk.StringVar()
                ballot_type_select = ttk.Combobox(vframe_0, textvariable=self.ballot_type_select_value)
                ballot_type_select['values'] = ('by_faction', 'by_name', 'secret')
                ballot_type_select.state(['readonly'])
                ballot_type_select.grid(column=2, row=0)
                # get() and set() for string, current for selected index or -1

                # row 1
                # [voteopt.entry,],+.button,-.button
                # todo, for now is assumend as ['ja', 'nein', 'enthaltung']

                # row 2
                # create.button
                ttk.Button(vframe_0, text='Create', command=self.create_ballot).grid(column=0, row=2, columnspan=4, sticky=tk.NE)

                # row 3
                self.ballots = {}   # notebook content dynamic
                self.ballots_notebook = ttk.Notebook(ctl_votes, padding="5 5 5 5")
                self.ballots_notebook.grid(column=0, row=1, sticky=(tk.N, tk.W, tk.E, tk.S))

            def build_tab_events():
                pass
            def build_tab_sergio():
                pass
            def build_tab_preferences():
                pass

            build_tab_members()
            build_tab_speakers()
            build_tab_votes()
            build_tab_events()
            build_tab_sergio()
            build_tab_preferences()

        # === window creations ===
        self.ctl = self.root = tk.Tk()
        self.ctl.title(f"Ronils control - {self.meeting.session_name}")
        self.ctl.columnconfigure(0, weight=1)
        self.ctl.rowconfigure(1, weight=1)

        self.view = tk.Toplevel(self.root)
        self.view.title(f"Ronils view - {self.meeting.session_name}")
        self.view.columnconfigure(0, weight=1)
        self.view.rowconfigure(0, weight=1)

        set_styles()
        load_icons()
        build_ctl()
        build_view_window()

        # === server start ===
        if self.server:
            self.server.run()
            # check every second for updates from hafee queue
            self.root.after(1000, self.check_server_queue)
            # print("DEBUG gui: server started")

        # === end of __init__ ===

    # view window functions

    def update_view(self):
        self.update_time(recursion=False)
        self.update_attendant_members()
        self.update_rqc()

    def update_time(self, recursion=True):
        # todo load time pattern from config
        self.time.set(strftime('%H:%M'))
        # self.time.set(strftime('%H:%M:%S'))
        if recursion:
            self.view_time.after(10000, self.update_time)

    def update_rqc(self):
        count = len(self.server.register_queue) if self.server else '---'
        text = f"waiting: {count}"
        self.register_queue_count.set(text)

    def update_attendant_members(self, faction_names=[]):
        if not faction_names:
            faction_names = self.meeting.factions.keys()
        factions_sized = self.meeting.sized_factions(offset=1)
        for f_name, f_attendant in factions_sized.items():
            fa_list = []
            for pos, person in enumerate(f_attendant):
                # todo clean pseudonyms up
                if person[0] in self.meeting.pseudonyms:
                    person = (self.meeting.pseudonyms[person[0]], person[1])
                # pos+1 because humans like to see stuff 1-based instead of 0-based
                fa_list.append(f"{pos+1}: {person[0]}")
            st = "\n".join(fa_list)
            self.attendant_members_strings[f_name].set(st)


    # member ctl tab and hafee server functions

    def check_server_queue(self):
        # print("DEBUG checking server queue")
        if not self.server:
            return
        try:
            received_data = self.server.q.get_nowait()
        except QEmpty:
            pass
        else:
            print(f"DEBUG q data in gui: {received_data}")
            person = self.server.person_lookup(received_data)
            if person:
                f_name = person[0]
                name = person[1]
                rank = person[2]
                newstate = (name, rank) not in self.meeting.attendant[f_name]
                self.members_state_change(newstate=newstate, persons=[person])
            else:
                self.server.register_queue_push(received_data)
                self.update_rqc()
            self.server.q.task_done()
        self.root.after(1000, self.check_server_queue)

    def get_selected_persons(self):
        f_name = self.ctl_members_factions.tab(self.ctl_members_factions.select(), 'text')
        ranks = [self.member_lut[f_name][selected] for selected in self.ctl_members_guilists[f_name].curselection()]
        # alternative code with rpartionen in case the rank shall be written behind a person,
        # e.g.: "Peter Arbeitsloser 42":
        # [self.ctl_members_guilists[f_name].get(selected).rpartition(' ') for selected in self.ctl_members_guilists[f_name].curselection()]
        persons = [(f_name, self.meeting.factions[f_name]['members'][rank], rank) for rank in ranks]
        return persons

    def register_queue_handle(self, person=None):
        if not person:
            persons = self.get_selected_persons()
            if not len(persons) == 1:
                return
            person = persons[0]
        self.server.register_queue_handle(person)
        self.update_rqc()
        f_name = person[0]
        name   = person[1]
        rank   = person[2]
        newstate = (name, rank) not in self.meeting.attendant[f_name]
        self.members_state_change(newstate=newstate, persons=[person])

    def register_queue_delete(self):
        self.server.register_queue_handle(None)
        self.update_rqc()

    def members_state_change(self, newstate=True, persons=None):
        """
        Arrival or leaving of persons. If no persons are given, selected persons from the interface are used.
        :param newstate: New state of the persons. True is arrived/attendant, False is leaved/absent.
        :param persons: None or list of 3-tuples identifying persons by faction, name, rank.
        """
        if not persons:
            persons = self.get_selected_persons()
        for p in persons:
            f_name = p[0]
            name = p[1]
            rank = p[2]
            if newstate:
                self.meeting.arrive((f_name, name, rank))
            else:
                self.meeting.leave( (f_name, name, rank))
        self.update_attendant_members()

    def members_arrive(self, persons=None):
        """
        Arrival of persons. If no persons are given, selected persons from the interface are used.
        :param persons: None or list of 3-tuples identifying persons by faction, name, rank.
        """
        self.members_state_change(newstate=True, persons=persons)

    def members_leave(self, persons=None):
        """
        Arrival of persons. If no persons are given, selected persons from the interface are used.
        :param persons: None or list of 3-tuples identifying persons by faction, name, rank.
        """
        self.members_state_change(newstate=False, persons=persons)

    def members_from_backup(self):
        """
        Load persons states from attendance file ('backup file').
        """
        self.meeting.arrive_from_backup()
        self.update_attendant_members()

    def export_attendance(self):
        """
        Export attendance file as md that can be used in the protocol.
        """
        self.meeting.export_attendance()    # todo add implicit language option dependent on gui language

    def quorum_satisfied(self, threshold=.5):
        # todo get threshold value from gui state
        self.update_attendant_members()
        satisfied = self.meeting.quorum_satisfied(threshold=threshold)
        self.ctl_output_string.set(f"Are >= {threshold} of the parliament attendent? {satisfied}!")

    def quorum_satisfied_23(self):
        self.quorum_satisfied(threshold=2/3)
        return

    # speaker ctl tab functions

    def speakers_push(self, person=None):
        # get value
        # self.meeting.speakers_push
        pass

    def speakers_pop(self, person=None):
        # get value
        # self.meeting.speakers_pop
        pass


    # vote ctl functions

    def create_ballot(self):

        def build_vote_tab():
            #voter.label [,vote_opt.spinbox]
            #save.button, correct.button, delete.button
            self.ballots[bid] = ttk.Frame(self.ballots_notebook, padding="5 5 10 10")
            self.ballots_notebook.add(self.ballots[bid], text=bid)

            # tab content
            # first row with vote opts
            ttk.Label(self.ballots[bid], text="$voter").grid(column=0, row=0, sticky=tk.NW)
            for col, vote_option in enumerate(vote_options, start=1):
                ttk.Label(self.ballots[bid], text=vote_option).grid(column=col, row=0, sticky=tk.NE)
            for row, voter in enumerate(voters, start=1):
                ttk.Label(self.ballots[bid], text=voter).grid(column=0, row=row, sticky=tk.NW)
                for col, vote_option in enumerate(vote_options, start=1):
                    # todo set proper upper bound 'to' depending on vote_type
                    sb = ttk.Spinbox(self.ballots[bid], from_=0, to=100, width=3)
                    sb.set(0)
                    sb.grid(column=col, row=row, sticky=tk.NE)

            # buttons
            # save button
            ttk.Button(self.ballots[bid], text='Speichern', command=self.save_vote_result).grid(column=0, row=len(voters)+1, sticky=tk.NW)
            return


        bid = self.ballot_id_entry_text.get()
        # todo get vote_per_person
        votes_per_person = 1
        vote_type = self.ballot_type_select_value.get()

        # get vote options
        vote_options=["ja", "nein", "Enthaltung"]
        if vote_type == 'secret' and not 'ungültig' in vote_options:
            vote_options.append('ungültig')

        voters, vote_options = self.meeting.new_ballot( ballot_id = bid,
                                                        vote_type = vote_type,
                                                        vote_options = vote_options,
                                                        votes_per_person = votes_per_person,
                                                        print_ballot = False)

        #print(voters)
        #print(vote_options)

        build_vote_tab()
        return

    def save_vote_result(self):
        result = []
        ballot_id = self.ballots_notebook.tab(self.ballots_notebook.select(), 'text')
        result.append(ballot_id)
        tmpsum = [0,0,0]
        # todo extend with vote_options
        for row, voter_label in enumerate(reversed(self.ballots[ballot_id].grid_slaves(column=0)[1:-1]), start=1):
            # this stupid thing iterates backwards
            # > grid_slaves: most recently managed first - https://tcl.tk/
            # todo rewrite this whole function. it grid_slaves behaves diffrently than expected
            voter = voter_label['text']
            # print(voter)
            result.append(voter)
            #voter.reverse()    # done in enumerate
            for idx, vote_spinbox in enumerate(reversed(self.ballots[ballot_id].grid_slaves(row=row)[:-1])):
                # this stupid thing iterates backwards
                # > grid_slaves: most recently managed first - https://tcl.tk/
                vote = vote_spinbox.get()
                result.append(vote)
                tmpsum[idx] += int(vote)
        print("\nDEBUG output:")
        print(tmpsum[0])
        print(tmpsum[1])
        print(tmpsum[2])
        todo = self.meeting.record_vote_result(ballot_id, result)
        # todo show result feedback in gui
        # return

    # system functions

    def run(self):
        self.update_time(recursion=True)
        self.update_rqc()
        self.root.mainloop()


if __name__ == "__main__":
    print("not runnable. import this file.")
