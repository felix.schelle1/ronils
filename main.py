#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Ronils is a meeting management tool.
It is developed to be used by the students committee of a university.
Ronils offers recording attendent persons, vote results and more.

git repository: https://gitlab.gwdg.de/felix.schelle1/ronils
No homepage or documentation yet.

Copyright (c) 2019-2022, Felix Schelle.
License: AGPLv3-or-later (see LICENSE for details)
"""

__author__ = 'Felix Schelle'
__version__ = '1.0.0'
__license__ = 'AGPLv3-or-later'

import argparse
import json
from time import strftime

import gui
import stupa
import hafee_server

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('parliament_data')
    parser.add_argument('-s', '--session', default='%Y-%m-%d')
    parser.add_argument('-c', '--config', default='config/conf_example.json')
    parser.add_argument('-v', '--version', action='version', version=f'%(prog)s {__version__}')
    args = parser.parse_args()
    # todo args description; on session name strftime is applied

    session_name = strftime(args.session)
    with open(args.parliament_data, 'r') as f:
        parliament_data = json.load(f)
    with open(args.config, 'r') as f:
        config = json.load(f)

    if 'hafee' in config.keys():
        server = hafee_server.Hafee_server(config=config['hafee'])
    else:
        server = None

    session = stupa.Stupa(
                session_name    = session_name,
                parliament_data = parliament_data,
                config          = config
              )
    interface = gui.Gui(session, server=server)
    interface.run()


if __name__ == "__main__":
    main()
