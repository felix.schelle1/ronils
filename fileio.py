#!/usr/bin/env python3

import argparse
import json, csv
import os

def sanatise_filename(filename):
    for sep in os.path.sep, os.path.altsep:
        if sep:
            filename = filename.replace(sep, '_')
    if filename[0] == '.':
        filename = "_" + filename
    return filename

def invoke_session(paths):
    if not os.path.isdir(paths['base']):
        try:
            os.makedirs(paths['base'])
        except OSError:
            print(f"Warning: Directory {paths['base']} does not exist and can not be created.")
    # todo check if need to create more files
    # base, protocol, votes, attendance

def invoke_hafee(filepath):
    if not os.path.exists(filepath):
        if not filepath.endswith('.json'):
            filepath += '.json'
        try:
            save_lut(filepath, {})
        except OSError:
            print(f"Error: hafee file {filepath} does not exist and can not be created.")
    return load_lut(filepath)

def arrive_from_backup(filepath):
    if not filepath.endswith('.csv'):
        filepath += '.csv'
    lines = []
    if os.path.exists(filepath):
        with open(filepath, 'r', newline='') as f:
            reader = csv.reader(f) 
            for line in reader:
                lines.append(line[1:])
    return lines


def record_attendance(timestamp, person, filepath, arriving=True):
    """
    Function to record arrivels and departures
    :param timestamp: time of event occurence
    :param person: 3-tuple identifying a person by faction, name, rank 
    :param filepath: path to file in which to save the event
    :param arriving: True if member is arriving, False if leaving
    :retun: None
    """
    if not filepath.endswith('.csv'):
        filepath += '.csv'

    f_name = person[0]
    member = person[1]
    rank   = person[2]

    with open(filepath, 'a+') as f:
        writer = csv.writer(f)
        writer.writerow([timestamp, f_name, member, str(rank), str(arriving)])
    return

def export_attendance(filepath, end='end'):
    """
    Exports attendance data from csv file to md file.
    :param filepath: Path to attendance file. Same filename is used for csv and md except extension.
    :param end: String used as leaving time if persons are listed as attendent in the end.
    :retun: None
    """
    # todo multiple language support
    if filepath.endswith('.csv'):
        path_source = filepath
        path_target = filepath[:-3] + 'md'
    elif filepath.endswith('.md'):
        path_source = filepath[:-2] + 'csv'
        path_target = filepath
    else:
        path_source = filepath + '.csv'
        path_target = filepath + '.md'

    lines = []
    with open(path_source, 'r', newline='') as f:
        reader = csv.reader(f)
        for line in reader:
            lines.append(line)

    events = {}
    for event in lines:
        time_HHMM = event[0][11:16]
        # substring containing HH:MM of perfect date time
        f_name = event[1]
        member_name = event[2]
        if not f_name in events:
            events[f_name] = {}
        if not member_name in events[f_name]:
            events[f_name][member_name] = []
        events[f_name][member_name].append((time_HHMM, event[4]=='True'))
        # True is read as string 'True'

    md_string = "# Anwesenheit {.unnumbered}\n\n"
    # unnumbered for toc ignore in pandoc template
    for f_name in events:
        md_string += f"**{f_name}:**\n\n|Name|Anwesenheitszeiten|\n|---|---|\n"
        for m_name in events[f_name]:
            md_string += f"| {m_name} |"
            not_first_time = False
            for data in events[f_name][m_name]:
                if not_first_time and data[1]:
                    md_string += " ;"
                not_first_time = True
                md_string += f" {data[0]}"
                if data[1]:
                    md_string += " -"
            if events[f_name][m_name][-1][1]:
                # member did not leave until end
                md_string += f" {end}"
            md_string += " |\n"
        md_string += "\n"
    with open(path_target, 'w+') as f:
        f.write(md_string)
    return

def record_vote(ballot_id, result, filepath):
    """
    Write result of a vote to vote file.
    :param filepath: directory where to save event file
    :retun: None
    """
    # todo rewrite this function to use json istead csv
    if not filepath.endswith('.csv'):
        filepath += '.csv'

    row = []
    row = result
    # print(result)
    #for vote in result:
    #    # todo maybe enforce type string here?
    #    row.extend(vote)

    with open(filepath, 'a+') as f:
        writer = csv.writer(f)
        writer.writerow(row)
    return

def vote_csv_to_md(path_source='', path_target=''):
    print("Not yet implemented")
    return


def load_lut(filepath):
    if not filepath.endswith('.json'):
        filepath += '.json'
    with open(filepath, 'r') as f:
        return json.load(f)

def save_lut(filepath, lut_data):
    if not filepath.endswith('.json'):
        filepath += '.json'
    with open(filepath, 'w') as f:
        json.dump(lut_data, f, indent=2, ensure_ascii=False)


# if __name__ == "__main__":
    # main()
