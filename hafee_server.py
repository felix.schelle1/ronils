#!/usr/bin/env python3

import socket
import threading, queue

import fileio

class Hafee_server:

    def __init__(self, config):
        self.host   = config['host']
        self.port   = config['port']
        self.secret = config['secret']
        self.lut_path = config['database']
        self.lut    = fileio.invoke_hafee(self.lut_path)

        self.register_queue = []

        self.q = queue.Queue()
        self.server = None
        # server thread is created in self.run

    def run(self):
        self.server = threading.Thread(target=self._start, daemon=True)
        self.server.start()
        """
        while not self.ready:
            print("DEBUG: waiting for server")
            time.sleep(0.1)
        """

    def _start(self):
        print(f"running hafee server on {self.host}:{self.port}")
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            s.bind((self.host, self.port))
            s.listen()
            while True:
                connection, client = s.accept()
                with connection as conn:
                    print(f"Connection from {client}")
                    val = ""
                    data = conn.recv(1024)
                    while data:
                        val += data.decode('utf-8')
                        print(f"DEBUG: received {val}")
                        data = conn.recv(1024)
                self.q.put(val)
            print("connection closed.")


    def person_lookup(self, person_hash):
        if person_hash in self.lut.keys():
            person = tuple(self.lut[person_hash])
        else:
            person = None
        return person

    def create_lut_entry(self, person_hash, person_data):
        if person_hash in self.lut.keys():
            print(f"Warning: {person_hash} already exists in database.")
            return
        self.lut[person_hash] = list(person_data)
        fileio.save_lut(self.lut_path, self.lut)

    def register_queue_push(self, person_hash):
        self.register_queue.append(person_hash)

    def register_queue_handle(self, person_data):
        """
        person_data = falsify: just remove hash and dont register
        """
        try:
            person_hash = self.register_queue.pop(0)
        except IndexError:
            print("Warning: register queue empty.")
        else:
            if person_data:
                self.create_lut_entry(person_hash, person_data)


if __name__ == "__main__":
    print("Starting dev server")
    conf = {
            "host": "127.0.0.1",
            "port": 4321,
            "secret": "secret",
            "database": "hafee_lut",
        }
    dev_server = Hafee_server(config=conf)
    dev_server.run()
